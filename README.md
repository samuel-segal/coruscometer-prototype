# Coruscometer Prototype


The Gitlab repo containing the Arduino code used for the Coruscometer prototype design for Johns Hopkins BMEI course.
A coruscometer is a device I designed that measures the magnitude and frequency of muscular tremors in a noninvasive and easy fashion. The user holds a rod on a laser, which serves as a large scale photointerrupter. The machine then uses infintesmal calculus to generate an index that appraises the user's muscular tremors, above which it sends a message for future testing. This index has been proven to be accurate across trials.
Given the noninvasive and accurate nature of the prototype alone, the coruscometer can likely be used in the future to get more accurate data from patients before they are diagnosed with neurodegenerative diseases such as Parkinsons. This data can then be used to provide earlier diagnoses, as well as aid our understanding in the onset of neurodegenerative symptoms.
The Arduino in question is the SunFounder board, which is an Uno.
