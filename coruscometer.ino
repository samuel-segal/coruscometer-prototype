//Imports the necessary libraries for the lcd display
#include <Wire.h>
//Note: You have to import this library through the IDE. Download the one by Marco Schwartz
#include <LiquidCrystal_I2C.h>

//The pin and lcd set-ups
const int laserPin = 12;
const int photoresistorPin = A0;
LiquidCrystal_I2C lcd(0x27,16,2);//Defines the lcd controller

const int threshold = 2500;
const int dt = 30;//The number of ms between trials
const int totalTime = 30 * 1000;//The total amount of time (30s)

int pastLuminance = 0; //Used to figure out the difference in luminance in getDeltaLuminance
int timeRemaining = totalTime;
boolean isContinue = true;//If it's false, the Arduino stops
float total = 0;//The integral summation


//Puts out the static variable labels used in the experiment
void printStatic(){
  lcd.setCursor(0,0);
  lcd.print("L");
  lcd.setCursor(0,1);
  lcd.print("L'");
  lcd.setCursor(12,0);
  lcd.print("Time");

  Serial.print("Dexterity Index: ");
  Serial.println(total);
}

//Prints out the data collected during the experiment
void printVariables(int luminance, float lumiDer, float timeValue){
  lcd.setCursor(3,0);
  lcd.print(luminance);
  
  lcd.setCursor(3,1);
  lcd.print(lumiDer);

  //Truncates the decimal. Not the cleanest method I know, but it works well enough
  lcd.setCursor(7,1);
  lcd.print("     ");

  lcd.setCursor(12,1);
  lcd.print((float)timeValue/1000);

  Serial.println(lumiDer);//This is used primarily for serial plotting
}

//Prints out finished and the dexterity index
void printFinal(){
  //Makes index independent from length of time
  float index = total/totalTime;
  lcd.clear();

  lcd.setCursor(4,0);

  lcd.print("Finished");

  
  delay(500);

  lcd.setCursor(0,1);
  lcd.print("D=");
  lcd.setCursor(2,1);
  lcd.print(index);

  delay(1000);
  //If the index if greater than the threshold
  if(index>threshold){
    lcd.setCursor(4,0);
    lcd.print("Danger! ");
  }
}

//Reads the amount of light hitting the photoresistor
int getLuminance(){
  return analogRead(photoresistorPin);
}

//Returns the difference in luminance since the last time the function was called
int getDeltaLuminance(int luminance){
  int output = luminance - pastLuminance;
  pastLuminance = luminance;
  return output;  
}

void setup() {

  lcd.init();
  lcd.backlight();
  
  pinMode(laserPin,OUTPUT);
  pinMode(photoresistorPin,INPUT);

  digitalWrite(laserPin,HIGH);

  printStatic();
  
  //Waits for all components to startup
  delay(1000);
  
  //Starts it up: not 100% accurate, but ultimately won't affect data
  pastLuminance = getLuminance();
  
  Serial.begin(9600);
}

void loop() {
  //If the measurements are still ongoing
  if(isContinue){
    int luminance = getLuminance();
    //Gets derative of luminance in simple fashion: change in luminance over change in time (seconds)
    int dl = getDeltaLuminance(luminance);
    float lightDer = (float)1000*dl / dt;

    //Used in place of abs, because there's a bug in that
    float absdl = lightDer > 0 ? lightDer : -lightDer;
    total += (1000*absdl);//This is the same as adding lightDer * dt, but more efficient.
    //It's abs so that opposite difference in luminance does not cancel out: shaking often occcurs that way
    Serial.print(total);
    Serial.print('\t');
    
    printVariables(luminance,lightDer,timeRemaining);
  
    timeRemaining -= dt;
    //Checks if the timer has expired
    if(timeRemaining<=0){
      printFinal(); 
      isContinue = false;//Ends experiment
    }
    delay(dt);
  }
}
